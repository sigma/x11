# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pam xorg [ suffix=tar.xz ] systemd-service
# Run eautoreconf to pick up patched XORG_PROG_RAWCPP macro
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="X Display Manager / XDMCP server"

LICENCES="X11"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    systemd
    ( libc: musl )
"

DEPENDENCIES="
    build:
        x11-proto/xorgproto
        x11-utils/util-macros[>=1.19.0-r1] [[
            note = [ Introduced a patch for XORG_PROG_RAWCPP to avoid the use of unprefixed cpp ]
        ]]
    build+run:
        sys-libs/pam
        x11-libs/libX11
        x11-libs/libXau
        x11-libs/libXaw
        x11-libs/libXdmcp
        x11-libs/libXext
        x11-libs/libXft
        x11-libs/libXmu
        x11-libs/libXt[>=1.0]
        x11-libs/libXinerama
        !libc:musl? ( dev-libs/libxcrypt:= )
        systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-ipv6
    --with-default-vt=vt7
    --with-pam
    --with-systemdsystemunitdir=no
    --with-xdmconfigdir=/etc/X11/xdm
    --without-selinux
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'systemd systemd-daemon'
)

src_install() {
    default

    pamd_mimic_system xdm auth account session

    install_systemd_files
}

