# Copyright 2008 Alexander Færøy <eroyf@eroyf.org>
# Copyright 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz ] cmake

SUMMARY="Open source alternative to the OpenGL toolkit library"

LICENCES="X11"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    ( X wayland ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        X? ( x11-proto/xorgproto )
    build+run:
        dev-libs/libglvnd[X?]
        x11-dri/glu
        X? (
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libXi
            x11-libs/libXrandr
            x11-libs/libXxf86vm
        )
        wayland? (
            sys-libs/wayland
            x11-libs/libxkbcommon
        )
"

# Install as libglut, not as libfreeglut. This preserves freeglut-2.4.0's behaviour,
# This is currently default behavior, but list it here in case that changes.
CMAKE_SRC_CONFIGURE_PARAMS=(
    -DFREEGLUT_BUILD_SHARED_LIBS:BOOL=TRUE
    -DFREEGLUT_BUILD_STATIC_LIBS:BOOL=FALSE
    -DFREEGLUT_REPLACE_GLUT:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'wayland FREEGLUT_WAYLAND'
)

