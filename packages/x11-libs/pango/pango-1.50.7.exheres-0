# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Copyright 2008 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2008-2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] meson

SUMMARY="A library for layout and rendering of text"
HOMEPAGE="https://www.pango.org/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv8 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    X [[ description = [ Xft backend ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
        gtk-doc? ( dev-doc/gi-docgen )
    build+run:
        dev-libs/fribidi[>=1.0.6]
        dev-libs/glib:2[>=2.62]
        media-libs/fontconfig[>=2.13.0]
        media-libs/freetype:2[>=2.1.5]
        x11-libs/cairo[>=1.12.10]
        x11-libs/harfbuzz[>=2.6.0][gobject-introspection?]
        X? (
            x11-libs/libXft[>=2.0.0]
            x11-libs/libXrender
        )
    test:
        fonts/cantarell-fonts
"

# Some tests fail due to changed rounding behaviour in harfbuzz
# See https://gitlab.gnome.org/GNOME/pango/-/issues/677
# Last checked with pango-1.50.5 and harfbuzz 4.0.0
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dcairo=enabled
    -Dfontconfig=enabled
    -Dfreetype=enabled
    -Dinstall-tests=false
    -Dlibthai=disabled
    -Dsysprof=disabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'X xft'
    'gobject-introspection introspection'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
)

