# Copyright 2013-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtMultimedia"
DESCRIPTION="Qt Multimedia is an essential module that provides a rich set of QML types
and C++ classes to handle multimedia content. It also provides necessary APIs to access
the camera and radio functionality."

LICENCES+=" GPL-2"
MYOPTIONS="
    alsa examples gstreamer pulseaudio
    qml [[ description = [ Support for QtQuick and the QML language ] ]]

    ( alsa gstreamer pulseaudio ) [[ number-selected = at-most-one ]]
    examples [[ requires = qml ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}][gui]
        alsa? ( sys-sound/alsa-lib[>=1.0.10] )
        examples? ( x11-libs/qtsvg:${SLOT}[>=${PV}] )
        gstreamer? (
            dev-libs/glib:2[>=2.26]
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-bad:1.0
            media-plugins/gst-plugins-base:1.0[gstreamer_plugins:opengl]
            x11-dri/mesa
        )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.10] )
        qml? ( x11-libs/qtdeclarative:${SLOT}[>=${PV}] )
"

qtmultimedia-6_src_configure()  {
    local cmake_params=(
        -DFEATURE_linux_v4l:BOOL=ON
        -DQT_NO_STRIP_WRAPPER:BOOL=TRUE

        $(cmake_option examples QT_BUILD_EXAMPLES)

        $(qt_cmake_feature alsa)
        $(qt_cmake_feature gstreamer gstreamer_1_0)
        $(qt_cmake_feature pulseaudio)

        $(cmake_disable_find qml Qt6Qml)
        $(cmake_disable_find qml Qt6Quick)
        $(cmake_disable_find qml Qt6QuickControls2)
        $(cmake_disable_find qml Qt6QuickTest)
    )

    ecmake "${cmake_params[@]}"
}

qtmultimedia-6_src_compile() {
    ninja_src_compile

    option doc && eninja docs
}

qtmultimedia-6_src_install() {
    ninja_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs

    if option examples ; then
        edo rmdir "${IMAGE}"/usr/share/qt6/examples/multimedia/spectrum \
            "${IMAGE}"/usr/share/qt6/examples/multimediawidgets/player
    fi
}

