# Copyright 2011 Paul Seidler
# Distributed under the terms of the GNU General Public License v2

SUMMARY="VA-API implementation for Intel G45 chipsets and Intel HD Graphics"
DESCRIPTION="
Supported chipsets: Cantiga (Intel GMA 4500MHD), Ironlake, Sandybridge, Ivybridge, Bay Trail,
Haswell, Broadwell, Cherryview/Braswell, Skylake, Broxton, Kabylake.
"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/vaapi"
DOWNLOADS="https://www.freedesktop.org/software/vaapi/releases/libva-${PN}/intel-vaapi-driver-${PV}.tar.bz2"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    X
    wayland
"

# failing, may depend on available hardware, last checked: 1.8.2
RESTRICT="test"

# automatic intel-gen4asm dependency isn't needed
DEPENDENCIES="
    build:
        dev-lang/python:=[>=2&<3]
        virtual/pkg-config
    build+run:
        x11-dri/libdrm[>=2.4.52][video_drivers:intel(+)]
        x11-libs/libva[>=1.7.3][X?][wayland?]
        X? (
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libXfixes
        )
        wayland? ( sys-libs/wayland[>=1.0.0] )
"

WORK=${WORKBASE}/intel-vaapi-driver-${PV}

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-drm
    --enable-hybrid-codec
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'X x11'
    wayland
)
DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-tests --disable-tests'
)

